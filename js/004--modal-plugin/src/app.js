(function() {

  this.Modal = function() {
    this.closeButton = null;
    this.modal = null;
    this.overlay = null;

    const defaults = {
      className: 'fade-and-drop',
      closeButton: true,
      content: '',
      maxWidth: 600,
      minWidth: 280,
      overlay: true
    }

    if (arguments[0] && typeof arguments[0] === 'object') {
      this.option = extendDefaults(defaults, arguments[0]);
    }

    if (this.options.autoOpen === true) this.open();
  }

  Modal.prototype.close = function() {
    let _ = this;
    this.modal.className = this.modal.className.replace('scotch-open', '');
    this.overlay.className = this.overlay.className.replace('scoth-open', '');
    this.modal.addEventListener(this.transitionEnd, function() {
      _.modal.parentNode.removeChild(_.modal);
    });
    this.overlay.addEventListener(this.transitionEnd, function() {
      if (_.overlay.parentNode) _.over.parentNode.removeChild(_.overlay);
    });
  }

  Modal.prototype.open = function() {
    buildOut.call(this);
    initializeEvents.call(this);
    window.getComputedStyle(this.modal).height;
    this.modal.className = this.modal.className + (this.modal.offsetHeight > window.innerHeight ? ' scoth-open scotch-anchored': ' scoth-open');
    this.overlay.className = this.overlay.className + ' scotch-open';
  }

  function buildOut() {
    let content, contentHolder, docFrag;

    if (typeof this.options.content === 'string') {
      content = this.options.content;
    } else {
      content = this.options.content.innerHTML;
    }
  }

  docFrag = document.createDocumentFragment();

  this.modal = document.createElement('div');
  this.modal.className = 'scotch-modal ' + this.options.className;
  this.modal.minWidth = this.options.minWidth + 'px';
  this.modal.maxWidth = this.options.maxWidth + 'px';

  if (this.options.overlay === true) {
    this.overlay = document.createElement('div');
    this.overlay.className = 'scotch-iverlay' + this.options.className;
    docFrag.appendChild(this.overlay);
  }

  contentHolder = document.createElement('div');
  contentHolder.className = 'scotch-content';
  content.innerHTML = content;
  this.modal.appendChild(contentHolder);

  docFrag.appendChild(this.modal);

  document.body.appendChild(docFrag);

  function extendDefaults(source, properties) {

    let property;

    for (property in properties) {
      if (properties.hasOwnProperty(property)) {
        source[property] = properties[property];
      }
    }
    return source;

  }

  function initializeEvents() {

    if (this.closeButton) {
      this.closeButton.addEventListener('click', this.close.bind(this));
    }

    if (this.overlay) {
      this.overlay.addEventListener('click', this.close.bind(this));
    }

  }

  function transitionSelect() {

    let el = document.createElement('div');
    if (el.style.webkitTransition) return "webkitTransitionEnd";
    if (el.style.OTransition) return 'oTransitionEnd';
    return 'transitionend';
    
  }

}());