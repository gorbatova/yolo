function isInteger(num) {
  return (num ^ 0) === num;
};

isInteger(4);
isInteger(-0.6);
isInteger(-5);
isInteger(1.4);
