// Lecture: if/else statements

var name = 'Valya';
var age = 23;
var isMarried = 'no';

if ( isMarried === 'yes' ) {
	console.log( name + ' is married!' );
} else {
	console.log( name + ' will hopefully marry soon :)' );
}

isMarried = true;

if ( isMarried ) {
	console.log( name + ' is married!' );
} else {
	console.log( name + ' will hopefully marry soon :)' );
}

isMarried ? console.log( 'YES' ) : console.log( 'NO' );