#### JavaScript History:
1. **1995** LiveScript;
2. **1996** LiveScript renamed to `JavaScript` (for marketing reasons);
3. **1997** ECMAScript 1 beacme the first version of the JS language standard;
3.1 ECMAScript — The language standard;
3.2 JavaScript — The language in practice;
4. **2009** ES5 (ECMAScript5) was released with lots of new features;
5. **2015** ES2015/**ES6** (ECMAScript 2015) wa released — *the biggest update ever*;
6. **2016** ECMAScript2016 (ES2016/ES7) was released with minor changes only;
7. **2017** ES2017 (ES8);

#### JavaScript Today:

##### ES5
1. Fully supported in all modern browsers;
2. Ready to be used today;

##### ES6
1. Full support coming soon!