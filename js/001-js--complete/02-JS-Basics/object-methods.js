// Lecture: object methods

var valya = {
	name: 'Valya',
	lastName: 'Gorbatova',
	yearOfBirth: 1995,
	isMarried: false,
	job: 'CSS Developer',
	family: [ 'Tanya', 'Alex' ],
	calculateAge: function () {
		// function expression (produces a value)
		return 2018 - this.yearOfBirth;
	}
};

// console.log( valya.calculateAge( 2018 ) ); // 23
console.log( valya.calculateAge() ); // 23
console.log( valya );

var age = valya.calculateAge();
valya.age = age;

console.log( valya );

// v2.0
var valyaTwo = {
	name: 'Valya',
	lastName: 'Gorbatova',
	yearOfBirth: 1995,
	isMarried: false,
	job: 'CSS Developer',
	family: [ 'Tanya', 'Alex' ],
	calculateAge: function () {
		this.age = 2018 - this.yearOfBirth;
	}
};

valyaTwo.calculateAge();
console.log( valyaTwo );

/*
Обекты могу содержать в себе функции,
которые (в свою очередь)
назваются методами (Methods).

Properties.
*/

// v3.0
var valyaThree = {
	name: 'Valya',
	lastName: 'Gorbatova',
	fullName: function( ) {
		this.fullName = this.name + ' ' + this.lastName;
	}
}

valyaThree.fullName( );
console.log( valyaThree );