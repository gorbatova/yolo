// Lecture: variables

var name = 'Valya';
console.log( name );

var lastName = 'Gorbatova';
console.log( lastName );

var age = 23;
console.log( age );

var fullAge = true;
console.log( fullAge );


// Lecture: variables 2
var name = 'Valya';
var age = 23;
console.log( name + " " + age, typeof( name + " " + age ) );
console.log( age + age, typeof( age + age ));

var job, isMarriend;
console.error( job );

job = 'CSS developer';
isMarriend = false;

console.log( job );

console.log( name + ' is a ' + age +
	' years old ' + job + '. Is she married? '
	+ isMarriend + '.' );

age = 'twenty three';

console.log( name + ' is a ' + age +
	' years old ' + job + '. Is she married? '
	+ isMarriend + '.' );

var lastName = prompt( 'What is the last name?', 'Valya' );
console.log( lastName );

alert( name + ' ' + lastName +' is a ' + age +
	' years old ' + job + '. Is she married? '
	+ isMarriend + '.' );
