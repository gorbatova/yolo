// Lecture: arrays

var names = [
	'John',
	'Jane',
	'Mark'
];

var years = new Array( 1990, 1969, 1848 );

//var animals = new Array( 'cat', 'dog' );
//console.log( animals );

names[1] = 'Ben';

console.log( names, names[2], typeof(names[1]) );
console.log( years );

var valya = [
	'Valya',
	'Gorbatova',
	1995,
	'CSS developer',
	false
];

// Data mutation:
// добавляет элемент в конце массива
valya.push( 'blue' );

// добавляет элемент в начало массива
valya.unshift( 'Mr.' );

// удаляет последний элемент массива
valya.pop();

// удаляет первый элемент массива
valya.shift();

// возвращает индекс элемента в массиве
// ели его нет —  возвращается -1
console.log( valya.indexOf( 1995 ) ); // 2

console.log( valya, typeof( valya ) ); // 'object'

if ( valya.indexOf( 'CSS developer' ) === -1 ) {
	console.log( 'Valya is not a CSS dev.' )
} else {
	console.log( 'She is!' )
}



