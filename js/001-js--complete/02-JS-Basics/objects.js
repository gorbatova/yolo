// Lecture: objects

var valya = {
	name: 'Valya',
	lastName: 'Gorbatova',
	yearOfBirth: 1995,
	isMarried: false,
	job: 'CSS Developer'
};

console.log( valya, typeof( valya ), valya.name, typeof( valya.yearOfBirth ) ); // 'object'
console.log( valya[ 'lastName' ] );

var xyz = 'job';
console.log( valya[xyz] ); // 'CSS developer'

// Data mutation:
valya.lastName = 'Cave';
valya[ 'job' ] = 'Front-end developer';

console.log( valya );

// Another way to create an object

var jane = new Object();
jane.name = 'Jane';
jane.lastName = 'Smith';
jane[ 'yearOfBirth' ] = 1951;
jane[ 'job' ] = 'retired';
jane.isMarried = true;

console.log( jane );
