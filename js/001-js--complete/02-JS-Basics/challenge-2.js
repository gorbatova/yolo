// Challenge 2

var CURRENT_YEAR = 2018;
var FULL_AGE = 18;

var years = [ 2001, 1234, 2018 ];

function printFullAge( years ) {
	var ages = new Array;
	var fullAges = new Array;


	for ( var i = 0; i < years.length; i++ ) {
		ages[ i ] = ( CURRENT_YEAR - years[i] );
	}

	for ( var i = 0; i < ages.length; i++ ) {
		if ( ages[ i ] >= FULL_AGE ) {
			console.log( 'Person ' + ( i + 1 ) + ' is ' + ages[ i ] + ' years ols, and is of full age' );
			fullAges.push( true );
		} else {
			console.error( 'Person ' + ( i + 1 ) + ' is ' + ages[ i ] + ' years ols, and is not of full age' );
			fullAges.push( false );
		}
	}

	return fullAges;

}

var years = [ 2001, 1234, 2018 ];
var full_1 = printFullAge( years );
var full_2 = printFullAge( [ 212, 1998, 1915, 1999 ] );
