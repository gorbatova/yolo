// Lecture: functions

// function statement, declaration (1)
function calculateAge( yearOfBirth ) {
	var age = 2018 - yearOfBirth;
	return age;
}

calculateAge( 1995 );
calculateAge( 1981 );
calculateAge( 2001 );

//function statement
function yearsUntillRetirement( name, year ) {
	var age = calculateAge( year );
	var retirement = 65 - age;
	if ( retirement >= 0  ) {
		console.log( name + ' retires in ' + retirement + ' years.' )
	} else {
		console.log( name + ' is already retired.' )
	}
}

yearsUntillRetirement( 'John', 1990 );
yearsUntillRetirement( 'Valya', 1995 );
yearsUntillRetirement( 'Nane', 1930 );

// function expression (2)
var someFun = function ( parameter ) {
	console.log( parameter )
}

someFun( 'HI' );

/*

Разница (1) и (2) в том, что
(1) - performs an action, does not produce a final value.
(2) - produces a value.

Основное отличие между ними: функции, объявленные как Function Declaration,
создаются интерпретатором до выполнения кода.

Если функция объявлена в основном потоке кода, то это Function Declaration.
Если функция создана как часть выражения, то это Function Expression.

*/