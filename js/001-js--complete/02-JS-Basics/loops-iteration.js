// Lecture: loops

// for loops
for ( var i = 0; i < 10; i++ ) {
	console.log( i );
}

var names = [
	'Valya', 'Kate', 'Sam', 'Mike'
];

for ( var i = 0; i < names.length; i++ ) {
	console.log( names[ i ] );
}

for ( var i = names.length - 1; i >= 0 ; i-- ) {
	console.log( names[ i ] );
}

// while loops
var i = 0;
while( i < names.length ) {
	console.log( names[ i ] );
	i++;
}

for ( var i = 1; i <= 5; i++ ) {
	console.log( i );

	if ( i === 3 ) {
		break;
		// 1
		// 2
		// 3
	}
}

for ( var i = 1; i <= 5; i++ ) {
	if ( i === 3 ) {
		continue;
		// skip 3
	}

	console.log( i );
	// 1
	// 2
	// 4
	// 5
}

