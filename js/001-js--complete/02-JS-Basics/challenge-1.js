// Challenge 1

var players = [];

var player_1 = {
	name: 'John',
	age: 260,
	height: 172
};

var player_2 = {
	name: 'Kate',
	age: 260,
	height: 172
};

var player_3 = {
	name: 'Sam',
	age: 260,
	height: 173
};

players.push( player_1, player_2, player_3 );
console.log( players );

var SCORE_CALC_PLAYER_1 = players[0].height + players[0].age * 5;
var SCORE_CALC_PLAYER_2 = players[1].height + players[1].age * 5;
var SCORE_CALC_PLAYER_3 = players[2].height + players[2].age * 5;

var msg = ' won with the total score amount : ';
if ( ( SCORE_CALC_PLAYER_1 > SCORE_CALC_PLAYER_2) && ( SCORE_CALC_PLAYER_1 > SCORE_CALC_PLAYER_3 ) ) {
	console.log( players[0].name + msg + SCORE_CALC_PLAYER_1 );
} else if ( ( SCORE_CALC_PLAYER_1 === SCORE_CALC_PLAYER_2 ) && ( SCORE_CALC_PLAYER_1 === SCORE_CALC_PLAYER_3 ) && ( SCORE_CALC_PLAYER_2 === SCORE_CALC_PLAYER_3 ) ) {
	console.log( ' It\'s a draw. ' );
} else if ( ( SCORE_CALC_PLAYER_2 > SCORE_CALC_PLAYER_1 ) && ( SCORE_CALC_PLAYER_2 > SCORE_CALC_PLAYER_3 ) )
	console.log( players[1].name + msg + SCORE_CALC_PLAYER_1 );
else {
	console.log( players[2].name + msg + SCORE_CALC_PLAYER_1 );
}