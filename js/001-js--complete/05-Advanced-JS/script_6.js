// Bind, call, apply

var john ={
	name: 'John',
	age: 26,
	job: 'teacher',
	presentation: function( style, timeOfDay ) {
		if ( style === 'formal' ) {
			console.log( 'Good ' + timeOfDay + ', Ladies and gentlemen! I\'m ' + this.name + '. I\'m a ' + this.job + ' and I\'m ' + this.age + ' years old.');
		} else if ( style === 'friendly' ) {
			console.log( 'Hey! What\'s up? I\'m ' + this.name + '. I\'m a ' + this.job + ' and I\'m ' + this.age + ' years old. Have a nice ' + timeOfDay + '.');
		}
	}
};

var emily = {
	name: 'Emily',
	age: 35,
	job: 'designer'
};

john.presentation( 'formal', 'morning' );
john.presentation.call( emily, 'friendly', 'afternoon' ); // Method borrowing
john.presentation.apply( emily, [ 'friendly', 'morning' ] ); // Apply method takes arguments as an array

// Bind doesn't immediately call a function but allows us to create a 'preset' of arguments
var johnFriendly = john.presentation.bind( john, 'friendly' );
johnFriendly( 'afternoon' );
johnFriendly( 'night' );

var emilyFormal = john.presentation.bind( emily, 'formal' );
emilyFormal( 'night' );








var years = [ 1990, 1965, 1937, 2005, 1998, 2018, 1999 ];

function arrayCalc( arr, fn ) {
	var arrRes = [];
	for ( var i = 0; i < arr.length; i++ ) {
		arrRes.push( fn( arr[ i ] ) );
	}
	return arrRes;
}

function calculateAge( el ) {
	var currentTime = new Date();
	var year = currentTime.getFullYear();
	return year - el;
}

function isFullAge( limit, el ) {
	return el >= limit;
}

var ages = arrayCalc( years, calculateAge );
var fullJapan = arrayCalc( ages, isFullAge.bind( this, 20 ) );
console.log( ages );
console.log( fullJapan );
