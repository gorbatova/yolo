// Closures

function retirement( retirementAge ) {
	var a = ' years left until retirement';
	return function ( yearOfBirth ) {
		var date = new Date();
		var year = date.getFullYear();
		var age = year - yearOfBirth;
		console.log( ( retirementAge - age ) + a);
	}
}

var retirementUS = retirement( 66 );
var retirementGermany = retirement( 65 );
var retirementIceland = retirement( 67 );

retirementGermany( 1995 );
retirementIceland( 1995 );
retirementUS( 1995 );
retirement( 66 )( 1995 );


// FROM script_3.js
function interviewQuestion( job ) {
	var designerQuestion = ' , can you please explain what UX design is?';
	var teacherQuestion = ', what subject do you teach?';
	var otherQuestion = ', what do you do?';
	return function( name ) {
		if ( job === 'designer' ) {
			console.log( name + designerQuestion );
		} else if ( job === 'teacher' ) {
			console.log( name + teacherQuestion );
		} else {
			console.log( name + otherQuestion );
		}
	}
}

interviewQuestion( 'designer' )( 'Valya' );
interviewQuestion( 'teacher' )( 'Kate' );
interviewQuestion( 'developer' )( 'Nana' );
