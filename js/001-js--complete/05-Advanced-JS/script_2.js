// Passing functions as arguments
var years = [ 1990, 1965, 1937, 2005, 1998, 2018 ];

function arrayCalc( arr, fn ) {
	// fn — callback function
	var arrRes = [];
	for ( var i = 0; i < arr.length; i++ ) {
		arrRes.push( fn( arr[ i ] ) );
	}
	return arrRes;
}

function calculateAge( el ) {
	var currentTime = new Date();
	var year = currentTime.getFullYear();
	return year - el;
}

function isFullAge( el ) {
	return el >= 18;
}

function maxHeartRate( el ) {
	if ( el >= 18 && el <= 81 ) {
		return Math.round( 206.9 - ( 0.67 * el ) );
	} else {
		return -1;
	}
}

var ages = arrayCalc( years, calculateAge );
var fullAges = arrayCalc( ages, isFullAge );
var rates = arrayCalc( ages, maxHeartRate );

console.log( ages );
console.log( fullAges );
console.log( rates );
