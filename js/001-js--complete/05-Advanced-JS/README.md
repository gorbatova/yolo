## Общее
- Конструктор (прототип) как Class в других ЯП;
- Каждый JS объект имеет свойство prototype, которыое делае возможным наследование в JS;
- Prototype property объекта — то место, куда мы помещаем методы и свойства, окторые хотим сделать доступными для наследования.

## Наследование и цепочка прототипов в консоли
```javascript
valya.hasOwnProperty( 'job' ) TRUE

valya.__proto__ === Person.prototype  // TRUE

valya.hasOwnProperty( 'job' ) // TRUE

valya.hasOwnProperty( 'lastName' ) // FALSE (NOT AN OWN PROPERTY)

valya instanceof Person // TRUE (т. к. мы создали объект valya через прототип Person)
```
## Primitives vs Objects

### Primitives
```javascript
var a = 24;
var b = a;
a = 46;
console.log( a ); // 46
console.log( b ); // 24
```

### Objects
```javascript
var obj1 = {
	name: 'John',
	age: 26
};

var obj2 = obj1;
obj1.age = 44;
console.log(obj1.age); // 44
console.log(obj2.age); // 44
```

### Functions
```javascript
var age = 27;
var obj = {
	name: 'valya',
	city: 'Moscow'
};

function change( a, b ) {
	a = 30;
	b.city = 'Nowhere';
}

change( age, obj );
console.log( age ); // 27
console.log( obj.city ); // Nowhere
```

> FUNCTIONS ARE ALSO OBJECTS IN JAVASCRIPT

## Immediately Invoked Function Expressions (IIFE)
```javascript
( function () {
	var score = Math.random() * 10;
	console.log( score >= 5 );
} )();
```

```javascript
( function ( goodLuck) {
	var score = Math.random() * 10;
	console.log( score >= 5 - goodLuck );
} )( 5 ); // passing an argument 'goodLuck'
```

## Closures (Замыкания)
```javascript
function retirement(retirementAge ) {
	var a = ' years left until retirement';
	return function ( yearOfBirth ) {
		var date = new Date();
		var year = date.getFullYear();
		var age = year - yearOfBirth;
		console.log( ( retirementAge - age ) + a );
	}
}

var retirementUS = retirement( 66 );
retirementUS( 1995 );

retirement( 66 )( 1995 );
```

## Bind, call, apply
