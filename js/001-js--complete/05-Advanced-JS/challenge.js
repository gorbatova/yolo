( function(){

	function Question( question, answers, correct) {
		this.question = question;
		this.answers = answers;
		this.correct = correct;
	}

	Question.prototype.displayQuestion = function( ) {
		console.log( this.question );

		for ( var i = 0; i < this.answers.length; i++ ) {
			console.log( i + ': ' + this.answers[ i ] );
		}
	};

	Question.prototype.checkAnswer = function( answer, callback ) {
		var sc;

		if ( answer === this.correct ) {
			console.log( 'Правильный ответ!' );
			sc = callback( true );
		} else {
			console.log( 'Неправильный ответ. Поробуй снова :)' );
			sc = callback( false );
		}

		this.displayScore( sc );
	};

	Question.prototype.displayScore = function( score ) {
		console.log( 'Ваш текущий счёт: ' + score );
		console.log( '------------------------------' );
	};

	var question_1 = new Question(
		'Правильное название професcии "web верстальщик"?' ,
		[ 'CSS инженер', 'Фронтендер', 'Разработчик web интерфейсов' ],
		0
	);

	var question_2 = new Question(
		'jQuery — это библиотека, покрывающая на 146% все нужны веб-разработки?',
		[ 'Нет', 'Да' ],
		1
	);

	var question_3 = new Question(
		'Что делает мышь?',
		['Скрам', 'Кродеться', 'Зарядку' ],
		1
	);

	var questions = [ question_1, question_2, question_3 ];

	function score() {
		var sc = 0;
		return function( correct ) {
			if ( correct ) {
				sc++;
			} 
			return sc;
		}
	}
	var keepScore = score();

	function nextQuestion() {

		var n = Math.floor( Math.random() * questions.length );
		questions[ n ].displayQuestion();

		var answer = prompt('Пожалуйста, выберите правильный ответ.');
		if(answer !== 'exit') {
				questions[n].checkAnswer(parseInt(answer), keepScore);
				nextQuestion();
		}
	}

	nextQuestion();

	// var n = Math.floor( Math.random() * questions.length );
	// questions[ n ].displayQuestion();
	// var answer = parseInt( prompt( 'Пожалуйста, выберите правильный ответ.' ) );
	// questions[ n ].checkAnswer( answer );
}());
