// Functions returning functions
function interviewQuestion(job ) {
	if ( job === 'designer' ) {
		return function( name ) {
			// anonymous function (doesn't have a name)
			console.log( name + ', can you please explain what UX design is?' );
		}
	} else if ( job === 'teacher' ) {
		return function( name ) {
			console.log( name + ', what subject do you teach?' )
		}
	} else {
		return function( name ) {
			console.log( 'Hello, ' + name + ', what do you do?' );
		}
	}
}

var designerQuestion = interviewQuestion( 'designer' );
var teacherQuestion = interviewQuestion( 'teacher' );

designerQuestion( 'Valya' );
teacherQuestion( 'Jane' );
teacherQuestion( 'Kim' );

interviewQuestion( 'teacher' )( 'Valya' );
