// Function constructor

var john = {
	name: 'John',
	yearOfBirth: 1990,
	job: 'teacher'
};

var Person = function( name, yearOfBirth, job ) {
	// Constructor name function starts with capital letter
	this.name = name;
	this.yearOfBirth = yearOfBirth;
	this.job = job;
};

Person.prototype.calculateAge = function() {
	// inheritance in practice
	console.log( 2018 - this.yearOfBirth )
};

Person.prototype.lastName = 'Smith';

// new creates new object
var john = new Person( 'john', 1990, 'teacher' );
var valya = new Person( 'valya', 1995, 'web developer' );

john.calculateAge();
valya.calculateAge();

console.log( valya.lastName );
console.log( john.lastName );
