// Lecture: scoping

var a = 'Hello!';
first( );

function first( ) {
	var b = 'Hi!';
	second( );

	function second( ) {
		var c = 'Hey!';
		console.log( a + b + c );
		// Hello!Hi!Hey!
		third( );
	}
}

function third( ) {
	var d = 'Valya';
	//console.log( a + b + c + d ); // b & c are not defined bc. this function is in a different scope
	console.log( a + d );
}