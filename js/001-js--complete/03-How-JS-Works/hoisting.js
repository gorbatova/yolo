// Lecture: hoisting

// functions
calculateAge( 2016 );

function calculateAge( year ){
	// function declaration
	console.log( 2018 - year );
}

// retirement( 1996 ); // retirement is not a function

var retirement = function( year ) {
	// function expression
	console.log( 65 - ( 2018 - year ) );
}

retirement( 1996 ); // 43

// variables
console.log( age ); // undefined — simply don't have a value yet
var age = 23;
console.log( age );

var name;
console.log( name );
name = 'Valya'; // undefined

function foo( ) {
	console.log( age ); // undefined
	var age = 65;
	console.log( age );
}
foo( ); // 65
console.log( age ); // 23 (line № 24; stored in a global execution context)