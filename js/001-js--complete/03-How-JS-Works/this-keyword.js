/// Lecture: the 'this' keyword in practice

var CURRENT_YEAR = 2018;

console.log( this ); // Window 'default' object

calculateAge( 1995 );

function calculateAge( year ) {
	console.log( CURRENT_YEAR - year );
	console.log( this ); // Window object
}

var valya = {
	name: 'Valya',
	yearOfBirth: 1995,
	calculateAge: function (  ) {
		console.log( this );
		console.log( CURRENT_YEAR - this.yearOfBirth );

		/*
		function innerFunction( ) {
			console.log( this );
		}
		innerFunction( ); // Window object. Still a regular function
		*/
	}
};

valya.calculateAge( );

var mike = {
	name: 'Mike',
	yearOfBirth: 1984
};

// Method borrowing
mike.calculateAge = valya.calculateAge;
mike.calculateAge( );