'use strict';

function countTax( price ){
	const taxRate = .73;

	if ( price > 0 ) {
		const msg = `Налог с продаж (${taxRate * 100} %), к оплате: ${ ( price / ( 1 + taxRate ) ).toFixed( 2 ) } Q `;
		console.log( msg );
	}
}

countTax( 20 );
countTax( -300 );
countTax( 202340 );
countTax( 7300 );
