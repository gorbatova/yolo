'use strict';

let itemName, itemPrice;

itemName = 'Телепорт бытовой VZHIH-101';
itemPrice = 10000;


// 2
(function log(){
	console.log( 'В наличии имеется: «' +  itemName + '»' );
	console.log( 'Стоимость товара ' + itemPrice + ' Q');
}());


// 3
function loyaltyProgram( itemsAmount ) {
	const discount = 10;

	if ( itemsAmount > 0 ) {
		if ( itemsAmount >= 2 ) {
			let itemsPrice = itemPrice * itemsAmount
			let itemsPriceDiscounted = itemsPrice - ( ( itemsPrice * 10 ) / 100 )
			console.log( "Цена покупки составит " + itemsPriceDiscounted + " Q" )
		} else {
			console.log( "Цена покупки составит " + itemPrice + " Q" )
			return itemPrice
		}
	} else return false


}

loyaltyProgram( -1 )
loyaltyProgram( 0 )
loyaltyProgram( 1 )
loyaltyProgram( 2 )
loyaltyProgram( 3 )
loyaltyProgram( 4 )


// 4
;(function bigShopping() {
	const budget = 52334224;
	const itemPrice = 6500;

	const x = budget % itemPrice
	const y = Math.floor( budget / itemPrice )

	console.log( 'Мы можем закупить ' + y + ' единиц товара, ' +
		'после закупки на счету останется ' + x + ' Q'  )

}());