'use strict';

const positions = [
	{
		title: 'Телепорт бытовой VZHIH-101',
		producer: {
			name: 'Рязанский телепортостроительный завод',
			deferPeriod: 10,
			lot: 3
		},
		price: 10000
	},
	{
		title: 'Ховерборд Mattel 2016',
		producer: {
			name: 'Волжский Ховерборд Завод',
			deferPeriod: 24,
			lot: 14
		},
		price: 9200
	},
	{
		title: 'Меч световой FORCE (синий луч)',
		producer: {
			name: 'Тульский оружейный комбинат',
			deferPeriod: 5,
			lot: 1
		},
		price: 57000
	}
];


// Задача № 1. Учет по партиям.
function lotCalculator( itemIndex, itemsAmount = 1 ) {
	try {
		const position = positions[itemIndex];
		const positionObj = new Object();

		positionObj.title = position.title;
		positionObj.producer = position.producer;
		positionObj.price = position.price;

		let result = new Object();
		if ( itemsAmount <= positionObj.producer.lot ) {
			result.lots = 1;
			result.total = itemsAmount * positionObj.price;
		} else if ( itemsAmount > positionObj.producer.lot ) {
			result.lots = Math.ceil( itemsAmount / positionObj.producer.lot );
			result.total = positionObj.producer.lot * result.lots * positionObj.price;
		}
		console.info( `${positionObj.title} ${itemsAmount} шт.: Заказать партий  ${result.lots}, стоимость ${result.total} Q` )
	} catch( e ) {
		console.error( e.name + "! " + e.message );
	}
}


lotCalculator( 2, 1 );
lotCalculator( 3, 20 );
lotCalculator( 1, 15 );
lotCalculator( 1, 14 );
lotCalculator( 0, 7 );
lotCalculator( 0, 4 );




// Задача № 2. Отсрочка платежа.

const deferedPayments = new Array();

function deferPay( producer, sum, date ) {
	console.log( ` ${producer} ${sum} ${date} ` )
}


