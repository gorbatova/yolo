'use strict';

let positions = [
	'Телепорт бытовой VZHIH-101',
	'Отвертка ультразвуковая WHO-D',
	'Ховерборд Mattel 2016',
	'Нейтрализатор FLASH black edition',
	'Меч световой FORCE (синий луч)'
];

let prices = [
	10000,
	4800,
	9200,
	2500,
	57000
];

let hitName = positions[2], hitPrice = prices[2];


// 1
const hit = {};

hit['price'] = hitPrice;
hit['name'] = hitName;

const showHit = () => {
	console.log( 'Хит продаж мартобря: <' + hit['name'] + '> цена ' + hit['price'] + 'Q' )
};

showHit();



//2
let items = [];
const index = 4;

for ( let i = 0; i < positions.length; i++ ) {
	const positionObj = {};
	
	positionObj.name = positions[i];
	positionObj.price = prices[i];
	
	items.push( positionObj );
}

console.log( `Купите ${items[index].name} по цене ${items[index].price} Q` );


// 3
const showDiscount = ( position, itemsAmount ) => {
	
	let discount, msg;
	
	const discountObject = { '0': .05, '10': .07, '50': .1, '100': .15 };
	
	if ( ( position >= 0) && ( itemsAmount >= 0  ) ) {
		
		switch ( true ) {
			case ( itemsAmount > 0 && itemsAmount < 10 ):
				discount = discountObject[0];
				break;
			case ( itemsAmount >= 10 && itemsAmount < 50 ):
				discount = discountObject[10];
				break;
			case ( itemsAmount >= 50 && itemsAmount < 100 ):
				discount = discountObject[50];
				break;
			case ( itemsAmount >= 100 ):
				discount = discountObject[100];
				break;
			default:
				discount = 0;
				break;
		}
		
		let totalCost = items[position].price * itemsAmount;
		let totalDiscount = totalCost - totalCost * discount;
		
		msg = `${items[position].name} — стоимость партии из ${itemsAmount} штук ${ totalDiscount } Q (скидка ${  parseInt( discount * 100 ) } %), ваша выгода ${ totalCost - totalDiscount } Q!`;
		
	} else if ( !(isNaN( position ) ) && !(isNaN( itemsAmount ) ) ) {
		msg = `Введите число`;
	} else {
		msg = `Нечего показать.`;
	}
	
	console.log( msg );

};

showDiscount( 0, 12 );
showDiscount( 3, 97 );
showDiscount( -3, 97 );
showDiscount( 1, 0 );
showDiscount( 1, -1 );
showDiscount( 'два', 2 );
showDiscount( 1, 'три' );


// 4
items[3].amount = 4;

const updateAmount = ( position, consumption = 1 ) => {
	let msg;
	if ( ( position >= 0 ) && ( position < items.length )) {
		let item = items[position];
		let name = item.name;
		if ( 'amount' in items[position] ) {
			let amount = item.amount;
			switch ( true ) {
				case ( ( amount === 0 ) ||  ( amount < consumption ) ):
					msg = `${name} закончился на складе.`;
					break;
				case ( amount > consumption ):
					amount -= consumption;
					msg = `${name} — остаток ${amount} шт.`;
					break;
				case ( amount === consumption ):
					msg = `Это был последний ${name}, вам повезло!.`;
					break;
				default:
					msg = `empty`;
					break;
			}
		} else return;
		console.log( msg );
	}
};


updateAmount( 1, 17 );
updateAmount( 3, 3 );
updateAmount( 3, 4 );
updateAmount( 3 );
updateAmount( -1 );