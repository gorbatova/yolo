' use strict ';

const positions = [
	'Отвертка ультразвуковая WHO-D',
	'Ховерборд Mattel 2016',
	'Нейтрализатор FLASH black edition',
	'Меч световой FORCE (синий луч)',
	'Машина времени DeLorean',
	'Репликатор домашний STAR-94',
	'Лингвенсор 000-17',
	'Целеуказатель электронный WAY-Y'
];

function itemsLog( logTitle ) {
	console.log( logTitle );
	positions.map( function( item, index ) {
		index++
		console.log( index + '\'' + item + '\'');
	} )
}

itemsLog( 'Список наименований' );

positions.push(
	'Экзоскелет Trooper-111',
	'Нейроинтерфейс игровой SEGUN',
	'Семена дерева Эйва'
);

itemsLog( 'Окончательный список наименований' );

console.log( positions );

let slicedArray = positions.slice(0, 5);

console.log( slicedArray );

slicedArray.forEach( function( arrayItem ) {
	console.log( arrayItem )
} );
