'use strict';

const check = ( restInStock, shoppingCartItems ) => {
	if ( ( restInStock > 0 ) && ( shoppingCartItems > 0 ) ) {
		if ( restInStock === shoppingCartItems ) {
			console.log( 'Вы забираете весь товар c нашего склада!' )
		} else if ( restInStock < shoppingCartItems ) {
			console.log( 'На складе нет такого количества товаров' )
		} else if ( restInStock > shoppingCartItems ) {
			console.log( 'Заказ оформлен' )
		}
	} else return
}

check(1, 1)
check(-2, 1)
check(-4, -10)
check(6, -1)
check(1, 6)
check(6, 1)




const checkRegion = ( region ) => {
	switch ( region ) {
		case 'Луна':
			console.log( 'Луна: 150 Q' );
			break;
		case 'Крабовидная туманность':
			console.log( 'Крабовидная туманность: 150 Q' );
			break;
		case 'Галактика Туманность Андромеды':
			console.log( 'Галактика Туманность Андромеды: 550 Q' );
			break;
		case 'Туманность Ориона':
			console.log( 'Туманность Ориона: 600 Q' );
			break;
		default:
			console.log( 'В ваш квадрант доставка не осуществляется' );
	}
}

checkRegion( 'Луна' );
checkRegion( 'Галактика' );