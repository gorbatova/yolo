## Graceful Degradation (Изящная деградация)

### Полифил

- JS based
- Автопрефиксер

### Progressive Enhancement (Прогрессивное улучшение)

Сначала разрабатывается простая раскладка (дефолтная). Потом идёт feature detection.

### Feature Detection

```javascript
const d = document.documentElement.style;
if ( ( 'flexWrap' in d ) || ( 'WebkitFlexWrap' in d ) || ( 'msFlexWrap' in d ) ) {
    // добавим нужный класс элементу
}
```

```css
@supports ( display: grid ) {
    /* цсс инжиниринг */ 
}
```

[source "Прогрессивные и деградирующие, Светлана Шарипова"](https://www.youtube.com/watch?v=9EbRFPVVHvk)